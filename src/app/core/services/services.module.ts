import { NgModule } from '@angular/core';
import { RoversService } from './rovers.service';

@NgModule({
  providers: [
    //RoversService
    {provide: RoversService, useClass: RoversService}
  ]
})
export class ServicesModule { }
